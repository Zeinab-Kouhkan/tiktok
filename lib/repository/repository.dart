import 'package:http/http.dart' as http;

class Repository {
  static final Repository _repository = Repository._();
  static const int _perPage = 10;

  Repository._();

  factory Repository() {
    return _repository;
  }

  Future<dynamic> getVideos({
    required int page,
  }) async {
    try {
      return await http.get(
        Uri.parse(
          'https://api.pexels.com/videos/search?query=nature&page=$page&per_page=$_perPage'
        ),
        headers:<String, String>{
          'Authorization':'563492ad6f9170000100000139c9463f491d4b809e068bdb4c75c52b'
        }
      );
    } catch (e) {
      return e.toString();
    }
  }
}