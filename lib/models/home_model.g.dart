// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeModel _$HomeModelFromJson(Map<String, dynamic> json) => HomeModel(
      json['id'] as int,
      json['url'] as String,
      json['name'] as String?,
      json['caption'] as String?,
      json['songName'] as String?,
      json['profileImg'] as String?,
      json['likes'] as String?,
      json['comments'] as String?,
      json['shares'] as String?,
      json['image'] as String,
    );
