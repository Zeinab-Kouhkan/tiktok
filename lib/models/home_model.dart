import 'package:json_annotation/json_annotation.dart';

part 'home_model.g.dart';

@JsonSerializable(createToJson: false)
class HomeModel {
  @JsonKey(name: 'id')
  int id;
  @JsonKey(name: 'url')
  String videoUrl;
  String? name;
  String? caption;
  String? songName;
  String? profileImg;
  String? likes;
  String? comments;
  String? shares;
  @JsonKey(name: 'image')
  String albumImg;

  HomeModel(this.id, this.videoUrl, this.name, this.caption, this.songName,
      this.profileImg, this.likes, this.comments, this.shares, this.albumImg);


  @override
  String toString() {
    return 'HomeModel{id: $id, videoUrl: $videoUrl,  albumImg: $albumImg}';
  }

  factory HomeModel.fromJson(Map<String, dynamic> json) =>
      _$HomeModelFromJson(json);
}
