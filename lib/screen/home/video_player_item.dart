import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tiktok/extensions/string.dart';
import 'package:tiktok/screen/home/right_panel.dart';
import 'package:video_player/video_player.dart';

import '../../entity/position.dart';
import '../../theme/colors.dart';
import '../../widgets/header_home_page.dart';
import '../../widgets/left_panel.dart';

class VideoPlayerItem extends StatefulWidget {
  const VideoPlayerItem(
      {Key? key,
      required this.videoUrl,
      required this.size,
      required this.id,
      required this.index})
      : super(key: key);
  final String videoUrl;
  final Size size;
  final String id;
  final int index;

  @override
  State<VideoPlayerItem> createState() => _VideoPlayerItemState();
}

class _VideoPlayerItemState extends State<VideoPlayerItem> {
  late VideoPlayerController _videoController;
  bool _isShowPlaying = false;
  late SharedPreferences _prefs;

  @override
  void initState() {
    super.initState();
    initSharedPreferences();
    _videoController = VideoPlayerController.asset(widget.videoUrl)
      ..initialize().then((value) async {
        PositionData? positionData = await getPosition();
        if (positionData != null && widget.id == positionData.id) {
          _videoController.seekTo(positionData.duration);
        }
        _videoController.play();
        setState(() {
          _isShowPlaying = false;
        });
      });
  }

  @override
  void dispose() {
    super.dispose();
    savePosition();
    _videoController.dispose();
  }

  void initSharedPreferences() async {
    _prefs = await SharedPreferences.getInstance();
  }

  void savePosition() async {
    if (_videoController.value.position != _videoController.value.duration) {
      _prefs.setString('position${widget.index % 2}',
          '${_videoController.value.position.toString()}/${widget.id}');
    }
  }

  Future<PositionData?> getPosition() async {
    final String? position = _prefs.getString(
      'position${widget.index % 2}',
    );
    return position?.positionData;
  }

  Widget isPlaying() {
    return _videoController.value.isPlaying && !_isShowPlaying
        ? Container()
        : Icon(
            Icons.play_arrow,
            size: 80,
            color: white.withOpacity(0.5),
          );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          if (_videoController.value.isPlaying) {
            savePosition();
            _videoController.pause();
          } else {
            _videoController.play();
          }
        });
      },
      child: RotatedBox(
        quarterTurns: -1,
        child: SizedBox(
            height: widget.size.height,
            width: widget.size.width,
            child: Stack(
              children: <Widget>[
                Container(
                  height: widget.size.height,
                  width: widget.size.width,
                  decoration: const BoxDecoration(color: black),
                  child: Stack(
                    children: <Widget>[
                      VideoPlayer(_videoController),
                      Center(
                        child: Container(
                          decoration: const BoxDecoration(),
                          child: isPlaying(),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: widget.size.height,
                  width: widget.size.width,
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 15, top: 20, bottom: 10),
                    child: SafeArea(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          const HeaderHomePage(),
                          Expanded(
                              child: Row(
                            children: <Widget>[
                              LeftPanel(
                                size: widget.size,
                                name: 'Vannak Niza🦋',
                                caption: 'Morning, everyone!!',
                                songName: 'original sound - Łÿ Pîkâ Ćhûû',
                              ),
                              RightPanel(
                                size: widget.size,
                                likes: '1.5M',
                                comments: '18.9K',
                                shares: '80K',
                                profileImg:
                                    'https://p16-tiktokcdn-com.akamaized.net/aweme/720x720/tiktok-obj/1663771856684033.jpeg',
                                albumImg:
                                    'images.unsplash.com/photo-1457732815361-daa98277e9c8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1650&q=80',
                              )
                            ],
                          ))
                        ],
                      ),
                    ),
                  ),
                )
              ],
            )),
      ),
    );
  }
}
