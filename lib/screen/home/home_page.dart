import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tiktok/screen/home/video_player_item.dart';

import '../../models/home_model.dart';
import '../../repository/repository.dart';
import 'bloc/home_bloc.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  late TabController _tabController;
  static const int targetIndex = 7;
  final List<HomeModel> videos = [];

  @override
  void initState() {
    super.initState();

    _tabController = TabController(length: videos.length, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return body();
  }

  Widget body() {
    var size = MediaQuery.of(context).size;
    return BlocProvider(
      create: (context) =>
          HomeBloc(repository: Repository())..add(const HomeFetchEvent()),
      child: Center(
        child: BlocConsumer<HomeBloc, HomeState>(
          listener: (context, state) {
            if (state is HomeLoadingState) {
              ScaffoldMessenger.of(context)
                  .showSnackBar(SnackBar(content: Text(state.message)));
            } else if (state is HomeSuccessState && state.videos.isEmpty) {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('No more videos')));
            } else if (state is HomeErrorState) {
              ScaffoldMessenger.of(context)
                  .showSnackBar(SnackBar(content: Text(state.error)));
              BlocProvider.of<HomeBloc>(context).isFetching = false;
            }
            return;
          },
          builder: (context, state) {
            if (state is HomeInitialState ||
                state is HomeLoadingState && videos.isEmpty) {
              return const CircularProgressIndicator();
            } else if (state is HomeSuccessState) {
              videos.addAll(state.videos);
              _tabController =
                  TabController(length: videos.length, vsync: this);
              BlocProvider.of<HomeBloc>(context).isFetching = false;
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
            } else if (state is HomeErrorState && videos.isEmpty) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  IconButton(
                    onPressed: () {
                      BlocProvider.of<HomeBloc>(context)
                        ..isFetching = true
                        ..add(const HomeFetchEvent());
                    },
                    icon: const Icon(Icons.refresh),
                  ),
                  const SizedBox(height: 15),
                  Text(state.error, textAlign: TextAlign.center),
                ],
              );
            }

            return RotatedBox(
              quarterTurns: 1,
              child: TabBarView(
                controller: _tabController
                  ..addListener(() {
                    if (_tabController.index == targetIndex &&
                        !BlocProvider.of<HomeBloc>(context).isFetching) {
                      BlocProvider.of<HomeBloc>(context)
                        ..isFetching = true
                        ..add(const HomeFetchEvent());
                    }
                  }),
                children: List.generate(videos.length, (index) {
                  return VideoPlayerItem(
                    videoUrl: 'assets/videos/video_${(index % 3) + 1}.mp4',
                    size: size,
                    id:videos[index].id.toString() ,
                    index: index,
                  );
                }),
              ),
            );
          },
        ),
      ),
    );
  }
}
