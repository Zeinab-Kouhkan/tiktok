part of 'home_bloc.dart';

abstract class HomeEvent  {
  const HomeEvent();
}

class HomeFetchEvent extends HomeEvent {
  const HomeFetchEvent();
}
