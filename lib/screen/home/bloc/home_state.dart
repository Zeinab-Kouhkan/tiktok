part of 'home_bloc.dart';

abstract class HomeState  {
  const HomeState();
}
class HomeInitialState extends HomeState {
  const HomeInitialState();
}

class HomeLoadingState extends HomeState {
  final String message;

  const HomeLoadingState({
    required this.message,
  });
}

class HomeSuccessState extends HomeState {
  final List<HomeModel> videos;

  const HomeSuccessState({
    required this.videos,
  });
}

class HomeErrorState extends HomeState {
  final String error;

  const HomeErrorState({
    required this.error,
  });
}




