import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;

import '../../../models/home_model.dart';
import '../../../repository/repository.dart';

part 'home_event.dart';

part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final Repository repository;
  int page = 1;
  bool isFetching = false;

  HomeBloc({required this.repository}) : super(const HomeInitialState()) {
    on<HomeEvent>((event, emit) async {
      if (event is HomeFetchEvent) {
        emit(const HomeLoadingState(message: 'Loading videos'));
        final response = await repository.getVideos(page: page);
        if (response is http.Response) {
          if (response.statusCode == 200) {
            final videos = jsonDecode(response.body)['videos'] as List;
            emit(HomeSuccessState(
              videos: videos.map((video) => HomeModel.fromJson(video)).toList(),
            ));
            page++;
          } else {
            emit(HomeErrorState(error: response.body));
          }
        } else if (response is String) {
          emit(HomeErrorState(error: response));
        }
      }
    });
  }
}
