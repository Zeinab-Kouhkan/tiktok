class PositionData{
  final String id;
  final Duration duration;

  PositionData(this.id, this.duration);

  @override
  String toString() {
    return 'PositionData{id: $id, duration: $duration}';
  }
}