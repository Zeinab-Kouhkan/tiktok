import 'package:flutter/material.dart';
import 'package:tiktok/root_app.dart';

 main()  {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MaterialApp(
    title: 'Tiktok',
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      primarySwatch: Colors.blue,
    ),
    home: const RootApp(),
  ));
}
