import '../entity/position.dart';

extension StringExtensions on String {
  PositionData get positionData {
    int hours = 0;
    int minutes = 0;
    int micros;
    List<String> main = split('/');
    String id=main[1];
    List<String> parts =main[0].split(':');
    if (parts.length > 2) {
      hours = int.parse(parts[parts.length - 3]);
    }
    if (parts.length > 1) {
      minutes = int.parse(parts[parts.length - 2]);
    }
    micros = (double.parse(parts[parts.length - 1]) * 1000000).round();
    return PositionData(id, Duration(hours: hours, minutes: minutes, microseconds: micros));
  }
}
